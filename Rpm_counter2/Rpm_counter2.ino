// read RPM

volatile int rpmcount = 0;      //se https://www.arduino.cc/reference/en/language/variables/variable-scope-qualifiers/volatile/
int rpm = 0;
unsigned long lastmillis = 0;
int level_pin = A0;             // Analog pinne att ansluta potentiometern till
int rpm_level = 0;              // Variabeln med det mappade gränsvärdet
int analog_value = 0;           // Tröskelvärdet som "varvtal" inte får understiga, om det sker ska matningsventilen stoppa matningen "deactivate_valve"
const byte interruptPin = 2;    // Definiera vilken pinne varvtalssignalen kommer in på, Uno har bara två möjliga kandidater
int valvePin = 8;               // Utgångspinne för framdrivningsventilen
int reversePin = 9;             // Utgångspinne för backningsventilen

void setup(){
 Serial.begin(9600); 
 pinMode(valvePin, OUTPUT);
 pinMode(reversePin, OUTPUT);
 attachInterrupt(digitalPinToInterrupt(interruptPin), rpm_fan, RISING);   // Interrupt(0) finns på pin(2).
}

void loop(){

 analog_value = analogRead(level_pin);
 rpm_level = map(analog_value, 1, 1023, 200, 600); // Mappar om potentiometerns värde( 1 - 1023 ) till ett värde som motsvarar varvtalets gränsvärde ( 200 - 600 rpm ) detta kan lätt ändras 

 
 if (millis() - lastmillis == 1000){  // Uppdatera en gång per sekund, detta blir frekvensen i Hertz
 
     detachInterrupt(0);   // Slå från avbrott under kalkyleringen
     
     rpm = rpmcount * 15;  // Konverterar frekvens till RPM, vi multiplicerar med 15 för att använda 4 pulser / varv, stabilare utgång trots några missade pulser här och var
     
    
    
     Serial.print("RPM Level= ");   // Skriver rubriken för tröskelvärdet
     Serial.print(rpm_level);       // Skriver det inställda tröskelvärdet
     Serial.print("\tRPM = ");      // Skriver en tab och sedan "RPM"
     Serial.print(rpm);             // Skriver själva värdet för rpm
     Serial.print("\t Hz= ");       // skriver en tab och sedan "Hz"
     Serial.print(rpmcount);        // Skriver frekvensen
    
       if (rpm < rpm_level){                    // Sjunker varvet under tröskelvärdet anropas deaktiveringsrutinen
          Serial.println("\tUtgång = 0\t");
          deactivate_valve();
          reverse_feed();
       }
       else{
          Serial.println("\tUtgång = 1\t");
          activate_valve();                     // annars kör vi på tillslag ( eventuellt skulle detta avsnitt göras om med en statusflagga för att bara anropa respektive funktion vid behov
          
       }
    
     
     rpmcount = 0;                            // Nollställ varvräknaren
     lastmillis = millis();                   // Uppdatera lastmillis
     attachInterrupt(0, rpm_fan, FALLING);    // Slå till avbrotten igen 
 }
}


void rpm_fan(){ // Här uppdateras rpmcount varje gång interrupt-pinnen triggas av stigande signal
  rpmcount++;
}


// Dessa två funktioner kan modifieras utifrån om man vill ha hög eller låg aktiv signal, 
// men nu visas status med en extern lysdiod


void activate_valve(){
  digitalWrite(valvePin, HIGH);     // Aktivera ventilen
}


void deactivate_valve(){            // Deaktivera ventilen
  digitalWrite(valvePin, LOW);
}


void reverse_feed(){
  digitalWrite(reversePin, HIGH);   // Aktivera ventilen för att backa matningen
  delay(3000);                      // Ange hur lång tid backning sker, detta anges i millisekunder därav 3000 (= 3 sekunder)
  digitalWrite(reversePin, LOW);    // Slå från backningen igen
}
